package ua.kyiv.heneraliuk.currencies

import android.app.Application
import android.content.Context
import ua.kyiv.heneraliuk.currencies.di.ApplicationComponent
import ua.kyiv.heneraliuk.currencies.di.ApplicationModule
import ua.kyiv.heneraliuk.currencies.di.DaggerApplicationComponent
import ua.kyiv.heneraliuk.currencies.work.SyncManager

class CurrenciesApp : Application() {
    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        injectMembers()
        SyncManager.startSync()
    }

    private fun injectMembers() = appComponent.inject(this)
}

fun Context.getAppComponent(): ApplicationComponent {
    return (applicationContext as CurrenciesApp).appComponent
}