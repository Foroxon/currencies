package ua.kyiv.heneraliuk.currencies.domain.usecases

import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import ua.kyiv.heneraliuk.currencies.domain.Repository
import ua.kyiv.heneraliuk.currencies.domain.usecases.UseCase.None
import ua.kyiv.heneraliuk.currencies.presentation.entity.Currency
import javax.inject.Inject

class SubscribeToCurrenciesUseCase
@Inject constructor(private val repo: Repository) : UseCase<Flowable<List<Currency>>, None> {

    override fun execute(params: None): Flowable<List<Currency>> {
        return repo.getCurrencies()
            .onErrorReturn { emptyList() }
            .observeOn(AndroidSchedulers.mainThread())
    }
}