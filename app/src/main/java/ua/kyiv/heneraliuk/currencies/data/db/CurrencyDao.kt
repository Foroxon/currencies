package ua.kyiv.heneraliuk.currencies.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Flowable
import ua.kyiv.heneraliuk.currencies.data.db.entity.CurrencyDb
import ua.kyiv.heneraliuk.currencies.data.db.entity.ExchangeDb

@Dao
interface CurrencyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCurrency(items: List<CurrencyDb>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertExchange(items: List<ExchangeDb>)

    @Query("SELECT * FROM currency ORDER BY name COLLATE LOCALIZED")
    fun getCurrencies(): Flowable<List<CurrencyDb>>

    @Query("SELECT * FROM exchange where currency_id = :currencyId ORDER BY exchange_date DESC")
    fun getExchangesForCurrency(currencyId: Long): Flowable<List<ExchangeDb>>
}