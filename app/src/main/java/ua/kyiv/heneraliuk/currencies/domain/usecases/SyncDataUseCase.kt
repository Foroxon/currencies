package ua.kyiv.heneraliuk.currencies.domain.usecases

import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import ua.kyiv.heneraliuk.currencies.domain.Repository
import ua.kyiv.heneraliuk.currencies.domain.usecases.UseCase.None
import javax.inject.Inject

class SyncDataUseCase
@Inject constructor(private val repo: Repository) : UseCase<Completable, None> {

    override fun execute(params: None): Completable {
        return repo.syncData()
            .observeOn(AndroidSchedulers.mainThread())
    }


}