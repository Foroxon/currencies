package ua.kyiv.heneraliuk.currencies.presentation.entity

data class Currency(
    val id: Long,
    val name: String,
    val code: String
)