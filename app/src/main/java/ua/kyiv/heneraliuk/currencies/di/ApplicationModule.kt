package ua.kyiv.heneraliuk.currencies.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import ua.kyiv.heneraliuk.currencies.BuildConfig
import ua.kyiv.heneraliuk.currencies.CurrenciesApp
import ua.kyiv.heneraliuk.currencies.data.db.AppDatabase
import ua.kyiv.heneraliuk.currencies.data.db.CurrencyDao
import ua.kyiv.heneraliuk.currencies.data.network.ApiClient
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: CurrenciesApp) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = application

    @Provides
    @Singleton
    fun provideDatabase(context: Context): AppDatabase = Room.databaseBuilder(
        context,
        AppDatabase::class.java, "currencies-db"
    )
        .build()

    @Provides
    fun provideDao(db: AppDatabase): CurrencyDao = db.getDao()

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://bank.gov.ua/NBUStatService/v1/")
            .client(createClient())
            .addConverterFactory(SimpleXmlConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    fun provideApiClient(retrofit: Retrofit): ApiClient = retrofit.create(ApiClient::class.java)

    private fun createClient(): OkHttpClient {
        val okHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC)
            okHttpClientBuilder.addInterceptor(loggingInterceptor)
        }
        return okHttpClientBuilder.build()
    }

}