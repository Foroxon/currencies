package ua.kyiv.heneraliuk.currencies.work

import androidx.work.*
import java.util.concurrent.TimeUnit

object SyncManager {
    private const val PERIODIC_WORK_TAG = "periodic_sync_work"

    fun startSync() {
        val periodicWork = PeriodicWorkRequestBuilder<SyncWork>(1, TimeUnit.HOURS)
            .addTag(PERIODIC_WORK_TAG)
            .setConstraints(buildConstraints())
            .build()
        WorkManager.getInstance()
            .enqueueUniquePeriodicWork(PERIODIC_WORK_TAG, ExistingPeriodicWorkPolicy.KEEP, periodicWork)
    }

    private fun buildConstraints(): Constraints {
        return Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
    }


}