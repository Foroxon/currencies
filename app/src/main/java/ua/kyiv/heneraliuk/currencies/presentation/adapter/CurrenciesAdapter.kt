package ua.kyiv.heneraliuk.currencies.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.currency_list_item.view.*
import ua.kyiv.heneraliuk.currencies.R
import ua.kyiv.heneraliuk.currencies.presentation.entity.Currency

class CurrenciesAdapter(private val onSelect: (currency: Currency) -> Unit) :
    ListAdapter<Currency, CurrenciesAdapter.ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.currency_list_item))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    inner class ViewHolder(private val rootView: View) : RecyclerView.ViewHolder(rootView) {
        fun bind(currency: Currency) {
            with(rootView) {
                setOnClickListener { onSelect(currency) }
                text.text = context.getString(R.string.currency_format, currency.name, currency.code)
            }
        }
    }

    private fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
        return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Currency>() {
            override fun areItemsTheSame(oldItem: Currency, newItem: Currency): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Currency, newItem: Currency): Boolean {
                return oldItem == newItem
            }
        }
    }
}