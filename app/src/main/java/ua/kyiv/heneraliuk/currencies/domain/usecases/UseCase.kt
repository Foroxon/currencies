package ua.kyiv.heneraliuk.currencies.domain.usecases

interface UseCase<out Result, in Params> where Result : Any{

    fun execute(params: Params): Result

    class None

}