package ua.kyiv.heneraliuk.currencies.presentation.entity

sealed class Error
object General : Error()
object NoInternet: Error()