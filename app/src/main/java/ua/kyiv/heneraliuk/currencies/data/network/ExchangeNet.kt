package ua.kyiv.heneraliuk.currencies.data.network

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(strict = false, name = "exchange")
data class ExchangeNet @JvmOverloads constructor(
    @field:ElementList(inline = true)
    var currencies: MutableList<CurrencyNet> = mutableListOf()
)

@Root(strict = false, name = "currency")
data class CurrencyNet @JvmOverloads constructor(
    @field:Element(name = "r030")
    var currencyId: Long = -1,
    @field:Element(name = "txt")
    var name: String= "",
    @field:Element(name = "rate")
    var rate: Float = 0f,
    @field:Element(name = "cc")
    var code: String = "",
    @field:Element(name = "exchangedate")
    var date: String = ""

)