package ua.kyiv.heneraliuk.currencies.domain.usecases

import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import ua.kyiv.heneraliuk.currencies.domain.Repository
import ua.kyiv.heneraliuk.currencies.domain.usecases.SubscribeToExchangesUseCase.Params
import ua.kyiv.heneraliuk.currencies.presentation.entity.Exchange
import javax.inject.Inject

class SubscribeToExchangesUseCase
@Inject constructor(private val repo: Repository) : UseCase<Flowable<List<Exchange>>, Params> {

    override fun execute(params: Params): Flowable<List<Exchange>> {
        return repo.getExchange(params.currencyId)
            .onErrorReturn { emptyList() }
            .observeOn(AndroidSchedulers.mainThread())
    }

    data class Params(val currencyId: Long)
}