package ua.kyiv.heneraliuk.currencies.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(
    tableName = "exchange",
    primaryKeys = ["currency_id", "exchange_date"]
)
data class ExchangeDb(
    @ColumnInfo(name = "currency_id")
    val currencyId: Long,

    @ColumnInfo(name = "exchange_date")
    val date: Long,

    @ColumnInfo(name = "exchange_rate")
    val rate: Float
)