package ua.kyiv.heneraliuk.currencies.presentation


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_exchange.*
import ua.kyiv.heneraliuk.currencies.R
import ua.kyiv.heneraliuk.currencies.presentation.model.CurrenciesViewModel
import java.text.SimpleDateFormat
import java.util.*


class ExchangeFragment : Fragment() {

    private val viewModel by lazy { ViewModelProviders.of(requireActivity()).get(CurrenciesViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.exchange.observe(this, Observer {
            empty_view.isVisible = false
            data_group.isVisible = true
            exchange_rate.text = String.format("%.3f", it.rate)
            currency_code.text = it.currencyId.toString()
        })
        viewModel.dates.observe(this, Observer { dates ->
            val dataAdapter = ArrayAdapter<String>(
                requireContext(),
                android.R.layout.simple_spinner_item, dates.map { it.toStringDate() }
            )
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            date_spinner.adapter = dataAdapter
            date_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    viewModel.setDate(dates[position])
                }
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_exchange, container, false)
    }

    private fun Long.toStringDate(): String {
        return SimpleDateFormat.getDateInstance().format(Date(this))
    }

    companion object {
        const val TAG = "exchange_fragment_tag"
    }

}
