package ua.kyiv.heneraliuk.currencies.work

import android.annotation.SuppressLint
import android.content.Context
import androidx.concurrent.futures.ResolvableFuture
import androidx.work.ListenableWorker
import androidx.work.WorkerParameters
import com.google.common.util.concurrent.ListenableFuture
import ua.kyiv.heneraliuk.currencies.domain.usecases.SyncDataUseCase
import ua.kyiv.heneraliuk.currencies.domain.usecases.UseCase
import ua.kyiv.heneraliuk.currencies.getAppComponent
import javax.inject.Inject

class SyncWork(appContext: Context, params: WorkerParameters) : ListenableWorker(appContext, params) {

    @Inject
    lateinit var syncUseCase: SyncDataUseCase

    init {
        applicationContext.getAppComponent().inject(this)
    }

    @SuppressLint("CheckResult")
    override fun startWork(): ListenableFuture<Result> {
        val result = ResolvableFuture.create<Result>()
        syncUseCase.execute(UseCase.None())
            .subscribe(
                { result.set(Result.success()) },
                { result.set(Result.failure()) }
            )
        return result
    }

}