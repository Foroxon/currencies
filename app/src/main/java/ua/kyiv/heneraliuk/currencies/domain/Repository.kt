package ua.kyiv.heneraliuk.currencies.domain

import android.annotation.SuppressLint
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import ua.kyiv.heneraliuk.currencies.data.db.CurrencyDao
import ua.kyiv.heneraliuk.currencies.data.db.entity.CurrencyDb
import ua.kyiv.heneraliuk.currencies.data.db.entity.ExchangeDb
import ua.kyiv.heneraliuk.currencies.data.network.ApiClient
import ua.kyiv.heneraliuk.currencies.data.network.CurrencyNet
import ua.kyiv.heneraliuk.currencies.presentation.entity.Currency
import ua.kyiv.heneraliuk.currencies.presentation.entity.Exchange
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class Repository @Inject constructor(private val dao: CurrencyDao, private val api: ApiClient) {

    fun syncData(): Completable {
        return api.getCurrencies()
            .subscribeOn(Schedulers.io())
            .map { it.currencies }
            .doOnSuccess { saveToDb(it) }
            .ignoreElement()
    }

    fun getCurrencies(): Flowable<List<Currency>> {
        return dao.getCurrencies()
            .subscribeOn(Schedulers.io())
            .map { it.toCurrency() }
    }

    fun getExchange(currencyId: Long): Flowable<List<Exchange>> {
        return dao.getExchangesForCurrency(currencyId)
            .subscribeOn(Schedulers.io())
            .map { it.toExchanges() }
    }

    private fun saveToDb(currencies: List<CurrencyNet>) {
        dao.insertCurrency(currencies.toCurrencyDb())
        dao.insertExchange(currencies.toExchangeDb())
    }


    private fun List<CurrencyNet>.toCurrencyDb(): List<CurrencyDb> {
        return map { CurrencyDb(it.currencyId, it.name, it.code) }
    }

    private fun List<CurrencyNet>.toExchangeDb(): List<ExchangeDb> {
        return map { ExchangeDb(it.currencyId, it.date.toTimestamp(), it.rate) }
    }

    private fun List<CurrencyDb>.toCurrency(): List<Currency> {
        return map { Currency(it.id, it.name, it.code) }
    }

    private fun List<ExchangeDb>.toExchanges(): List<Exchange> {
        return map { Exchange(it.currencyId, it.date, it.rate) }
    }

    @SuppressLint("SimpleDateFormat")
    private fun String.toTimestamp(): Long {
        return try {
            SimpleDateFormat("dd.MM.yyyy").parse(this).run {
                val cal = Calendar.getInstance(TimeZone.getTimeZone("EET"))
                cal.time = this
                cal.set(Calendar.HOUR_OF_DAY, 0)
                cal.set(Calendar.MINUTE, 0)
                cal.set(Calendar.SECOND, 0)
                cal.set(Calendar.MILLISECOND, 0)
                cal.timeInMillis
            }
        } catch (ex: Exception) {
            System.currentTimeMillis()
        }
    }
}