package ua.kyiv.heneraliuk.currencies.presentation.entity

data class Exchange(
    val currencyId: Long,
    val date: Long,
    val rate: Float
)