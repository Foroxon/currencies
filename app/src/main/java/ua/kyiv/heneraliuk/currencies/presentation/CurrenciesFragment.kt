package ua.kyiv.heneraliuk.currencies.presentation


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_currencies.*
import ua.kyiv.heneraliuk.currencies.R
import ua.kyiv.heneraliuk.currencies.presentation.adapter.CurrenciesAdapter
import ua.kyiv.heneraliuk.currencies.presentation.entity.General
import ua.kyiv.heneraliuk.currencies.presentation.entity.NoInternet
import ua.kyiv.heneraliuk.currencies.presentation.model.CurrenciesViewModel

class CurrenciesFragment : Fragment() {

    private val viewModel by lazy { ViewModelProviders.of(requireActivity()).get(CurrenciesViewModel::class.java) }
    private val adapter by lazy {
        CurrenciesAdapter {
            viewModel.selectCurrency(it)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.currencies.observe(this, Observer {
            adapter.submitList(it)
            empty_view.isVisible = it.isEmpty()
        })
        viewModel.loading.observe(this, Observer {
            refresh.isRefreshing = it
        })

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_currencies, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler.adapter = adapter
        refresh.setOnRefreshListener { viewModel.update() }
        viewModel.setErrorListener {
            Snackbar.make(
                view, when (it) {
                    General -> R.string.general_error
                    NoInternet -> R.string.no_internet
                }, Snackbar.LENGTH_LONG
            ).show()
        }
    }

    companion object {
        const val TAG = "currency_fragment_tag"
    }
}
