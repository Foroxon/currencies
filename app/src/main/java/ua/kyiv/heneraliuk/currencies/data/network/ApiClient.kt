package ua.kyiv.heneraliuk.currencies.data.network

import io.reactivex.Single
import retrofit2.http.GET

interface ApiClient {
    @GET("statdirectory/exchange")
    fun getCurrencies(): Single<ExchangeNet>
}