package ua.kyiv.heneraliuk.currencies.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_main.*
import ua.kyiv.heneraliuk.currencies.R
import ua.kyiv.heneraliuk.currencies.presentation.model.CurrenciesViewModel

class MainActivity : AppCompatActivity() {

    private val viewModel by lazy { ViewModelProviders.of(this).get(CurrenciesViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel.setNavigationListener {
            if (container != null) {
                supportFragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.container, ExchangeFragment(), ExchangeFragment.TAG)
                    .commit()
            }
        }
        if (!isFragmentsAdded() && container != null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, CurrenciesFragment(), CurrenciesFragment.TAG)
                .commit()
        }

        if (supportFragmentManager.backStackEntryCount > 0) {
            onBackPressed()
        }
    }

    private fun isFragmentsAdded(): Boolean {
        return supportFragmentManager.findFragmentByTag(CurrenciesFragment.TAG) != null
                || supportFragmentManager.findFragmentByTag(ExchangeFragment.TAG) != null
    }
}
