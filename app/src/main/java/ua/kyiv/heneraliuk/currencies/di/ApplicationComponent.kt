package ua.kyiv.heneraliuk.currencies.di

import dagger.Component
import ua.kyiv.heneraliuk.currencies.CurrenciesApp
import ua.kyiv.heneraliuk.currencies.data.db.AppDatabase
import ua.kyiv.heneraliuk.currencies.data.network.ApiClient
import ua.kyiv.heneraliuk.currencies.presentation.model.CurrenciesViewModel
import ua.kyiv.heneraliuk.currencies.work.SyncWork
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {
    fun inject(application: CurrenciesApp)
    fun inject(worker: SyncWork)
    fun inject(viewModel: CurrenciesViewModel)

}