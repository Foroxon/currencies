package ua.kyiv.heneraliuk.currencies.presentation.model

import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.disposables.Disposable
import ua.kyiv.heneraliuk.currencies.CurrenciesApp
import ua.kyiv.heneraliuk.currencies.domain.usecases.SubscribeToCurrenciesUseCase
import ua.kyiv.heneraliuk.currencies.domain.usecases.SubscribeToExchangesUseCase
import ua.kyiv.heneraliuk.currencies.domain.usecases.SyncDataUseCase
import ua.kyiv.heneraliuk.currencies.domain.usecases.UseCase
import ua.kyiv.heneraliuk.currencies.presentation.entity.*
import java.net.UnknownHostException
import javax.inject.Inject

class CurrenciesViewModel(app: Application) : AndroidViewModel(app) {

    @Inject
    lateinit var syncUseCase: SyncDataUseCase
    @Inject
    lateinit var getCurrenciesUseCase: SubscribeToCurrenciesUseCase
    @Inject
    lateinit var getExchangesUseCase: SubscribeToExchangesUseCase

    val currencies: LiveData<List<Currency>> = MutableLiveData()
    val dates: LiveData<List<Long>> = MutableLiveData()
    val exchange: LiveData<Exchange> = MutableLiveData()
    val loading: LiveData<Boolean> = MutableLiveData<Boolean>()
        .apply {
            value = false
        }

    private var currencyDisposable: Disposable? = null
    private var exchangeDisposable: Disposable? = null
    private var selectedCurrency: Currency? = null
    private var errorListener: ((error: Error) -> Unit)? = null
    private var navigationListener: (() -> Unit)? = null
    private val exchangeMap: MutableMap<Long, Exchange> = mutableMapOf()


    init {
        getApplication<CurrenciesApp>().appComponent.inject(this)
        currencyDisposable = getCurrenciesUseCase.execute(UseCase.None())
            .subscribe {
                if (selectedCurrency == null && it.isNotEmpty()) {
                    setSelectedCurrency(it.first())
                }
                setCurrencies(it)
            }
    }

    fun selectCurrency(currency: Currency) {
        setSelectedCurrency(currency)
        navigationListener?.invoke()
    }

    @SuppressLint("CheckResult")
    fun update() {
        syncUseCase.execute(UseCase.None())
            .doOnSubscribe { setLoading(true) }
            .doOnTerminate { setLoading(false) }
            .subscribe({}, {
                errorListener?.invoke(
                    when (it) {
                        is UnknownHostException -> NoInternet
                        else -> General
                    }
                )
            })
    }

    fun setErrorListener(listener: (error: Error) -> Unit) {
        errorListener = listener
    }

    fun setNavigationListener(listener: () -> Unit) {
        navigationListener = listener
    }

    fun setDate(date: Long) {
        exchangeMap[date]?.let {
            setExchange(it)
        }
    }

    private fun setSelectedCurrency(currency: Currency) {
        if (selectedCurrency == currency) return
        selectedCurrency = currency
        subscribeToExchanges()
    }

    private fun setCurrencies(newCurrencies: List<Currency>) {
        (currencies as MutableLiveData<List<Currency>>).value = newCurrencies
    }

    private fun setDates(newDates: List<Long>) {
        (dates as MutableLiveData<List<Long>>).value = newDates
    }

    private fun setExchange(newExchanges: Exchange) {
        (exchange as MutableLiveData<Exchange>).value = newExchanges
    }

    private fun setLoading(newLoading: Boolean) {
        (loading as MutableLiveData<Boolean>).value = newLoading
    }

    private fun subscribeToExchanges() {
        exchangeDisposable?.dispose()
        exchangeDisposable = getExchangesUseCase
            .execute(SubscribeToExchangesUseCase.Params(selectedCurrency!!.id))
            .subscribe { exchanges ->
                exchangeMap.clear()
                exchangeMap.putAll(exchanges.map { it.date to it }.toMap())
                setDates(exchanges.map { it.date })
                if (exchanges.isNotEmpty()) {
                    setExchange(exchanges.first())
                }
            }
    }

    override fun onCleared() {
        currencyDisposable?.dispose()
        exchangeDisposable?.dispose()
        super.onCleared()
    }
}