package ua.kyiv.heneraliuk.currencies.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import ua.kyiv.heneraliuk.currencies.data.db.entity.CurrencyDb
import ua.kyiv.heneraliuk.currencies.data.db.entity.ExchangeDb

@Database(entities = [CurrencyDb::class, ExchangeDb::class],
    version = 1,
    exportSchema = false)
abstract class AppDatabase: RoomDatabase() {

    abstract fun getDao() : CurrencyDao
}